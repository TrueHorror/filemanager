# File System Manager in C#

## Intro

This Console program gives the user some control over what they want to do with some files.

### Main Menu

The main menu is created with a custom `AppMenu` class. It is essentially a Dictionary but formatted for console application menus.

When the program starts, the user will be presented with the main menu which gives the user 4 options:
You can:

1. List every file in the `/resources` directory.
   - Uses `DirectoryInfo` class from C# to create a list of `FileInfo` objects of all the files in the directory, then a `foreach` and `FileInfo.Name`.
2. List every file with a specified extension that the user searches for.
   - Prompts the user to enter a file extension that will be used to match files from a list returned from `DirectoryInfo`
3. Manipulate a `'.txt'` file called `Dracula.txt`.
   - Takes the user to a the Dracula file menu.
4. Exit the program.

### Dracula file Menu

This menu is also created with `AppMenu`.

The user can now:

1. Get the file name of the Dracula file.
   - Uses `FileInfo` with a specified path (where `dracula.txt` is located).
2. Get the size of the file.
   - Also uses `FileInfo` to get the size. `FileInfo.Length` which gives the size in bytes.
3. Get amount of lines in the file.
   - A `StreamReader.ReadLine` in a while loop that counts up `ìnt lines` for every line in the file.
4. Search for a word in the file and prints occurrences.
   - Prompts the user to enter a search term. Then inside a `StreamReader.ReadLine` while loop, we split the lines in `Dracula.txt` on space (`" "`) and for each word in the created array after the split, formats the word with `Regex.Replace` with pattern `'[^a-zA-Z]+'` that only accepts normal characters (not special and digits).
   - After, we check each formatted word if it matches the search term. If it does, we increment a count and prints that.
