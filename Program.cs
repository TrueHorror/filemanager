﻿using FileManager.Menu;
using FileManager.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FileManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Beep();
            MainMenuPrompt();
        }

        private static void MainMenuPrompt() // Can call this later in the program to take user back to mainmenu
        {
            List<string> menuOptions = new List<string>();
            menuOptions.Add("List all files in resource directory.");
            menuOptions.Add("Get files based on extension");
            menuOptions.Add("Manipulate Dracula.txt file");
            menuOptions.Add("Exit program");

            // Is acctually just a Dictionary, but formatted for console menues with int and string ex:("1. Option")
            AppMenu menu = new AppMenu(menuOptions.Count, menuOptions); 

            Console.WriteLine("File manager menu:");
            MenuGenerator(menu);
            Console.Write("Enter option: ");


            string menuInput = Console.ReadLine();
            bool success = MenuValidation(menuOptions.Count, menuInput); // Checks if menuInput can be parsed to int with check for out of bound menu option select

            if (!success)
            {
                MainMenuPrompt();
            }
            else
            {
                GoToSelectedOption(int.Parse(menuInput)); // Wierd hack, but works
            }

        }

        private static void MenuGenerator(AppMenu menu)
        {
            foreach (var option in menu.MenuDictionary)
            {
                Console.WriteLine(option.Key + ". " + option.Value);
            }
        }

        private static bool MenuValidation(int options, string menuInput)
        {
            int selectedOption;

            if (int.TryParse(menuInput, out selectedOption))
            {
                if (selectedOption > options)
                {
                    Console.WriteLine("That is no option, try again!");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                Console.WriteLine("The input has to be a number, try again!");
                return false;
            }
        }

        private static void GoToSelectedOption(int selectedOption)
        {
          
            
            switch (selectedOption)
            {
                case 1:
                    Stopwatch sw = LoggingService.StartLogging(); // A logging Stopwatch starts as soon as a user has selected a option
                    FileService.ListAllFiles();
                    LoggingService.StopLogging(sw); // stops the stopwatch and prints elepsed time after an operation
                    break;
                case 2:
                    Console.WriteLine("What extension do you wish to list? ('.png', '.txt', '.jpeg', '.jpg', '.jfif')");
                    Console.Write(": ");
                    string extension = Console.ReadLine();

                    Stopwatch swExtension = LoggingService.StartLogging(); 

                    bool success = validateExtensionString(extension); // Checks if extension input axists in array with strings of extensions 
                    if (success)
                    {                       
                        FileService.ListSpecificExtension(extension);
                        LoggingService.StopLogging(swExtension);
                    }
                    else
                    {
                        Console.WriteLine("File extension doesn't exist. Try again... ");
                        GoToSelectedOption(selectedOption);
                    }
                    break;

                case 3:
                    ManipulateDraculaTxt();
                    break;

                case 4:
                    Environment.Exit(0);
                    break;

                default:
                    break;

            }
            
            BackToMenu(); 
        }

        private static bool validateExtensionString(string extension)
        {
            string[] extensions = new string[] { ".png", ".txt", ".jpeg", ".jpg", ".jfif" };
            return extensions.Contains(extension);
            
        }

        private static void BackToMenu()
        {
            Console.WriteLine("Back to menu? (y/n)\n (selecting 'n' will exit the program)");
            string selected = Console.ReadLine();

            switch (selected)
            {
                case "y":
                    MainMenuPrompt();
                    break;
                case "n":
                    break;
                default:
                    Console.WriteLine("Please select 'y' or 'n'");
                    BackToMenu();
                    break;
            }
        }

        private static void ManipulateDraculaTxt()
        {
            List<string> menuOptions = new List<string>();
            menuOptions.Add("Get file name.");
            menuOptions.Add("Get file size.");
            menuOptions.Add("Get lines of text.");
            menuOptions.Add("Search for word."); // ignore case, list count (binary tree?)
            
    

            AppMenu draculaMenu = new AppMenu(menuOptions.Count, menuOptions);

            Console.WriteLine("These are you option for file manipulation:");
            MenuGenerator(draculaMenu);
            Console.Write(": ");
            string input = Console.ReadLine();
            bool success = MenuValidation(menuOptions.Count, input);
            if (!success)
            {
                ManipulateDraculaTxt();
            }
            else
            {
                DraculaSelected(int.Parse(input));
            }

        }

        private static void DraculaSelected(int input)
        {
            
            switch (input)
            {
                case 1:
                    Stopwatch sw = LoggingService.StartLogging();
                    FileService.GetFileName();
                    LoggingService.StopLogging(sw);
                    break;
                case 2:
                    Stopwatch swSize = LoggingService.StartLogging();
                    FileService.GetFileSize();
                    LoggingService.StopLogging(swSize);
                    break;
                case 3:
                    Stopwatch swLines = LoggingService.StartLogging();
                    FileService.GetTextLines();
                    LoggingService.StopLogging(swLines);
                    break;
                case 4:
                    FileService.SearchTerm();
                    break;
                default:
                    break;
            }
        }
    }
}
