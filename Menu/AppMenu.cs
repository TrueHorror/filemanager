﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.Menu
{
    class AppMenu
    {
        Dictionary<int, string> menuDictionary;

        public Dictionary<int, string> MenuDictionary { get => menuDictionary; set => menuDictionary = value; }

        // Setting up a dictionary for a menu, can be used in multiple projects later
        public AppMenu(int menuSize, List<string> menuOptions)
        {
            MenuDictionary = new Dictionary<int, string>();
            for (int option = 1; option <= menuSize; option++)
            {
                MenuDictionary.Add(option, menuOptions[option-1]);
            }


        }
    }
}
