﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace FileManager.Services
{
    class LoggingService
    {
        public static Stopwatch StartLogging()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            return sw;
        }

        public static void StopLogging(Stopwatch sw)
        {
            sw.Stop();

            TimeSpan ts = sw.Elapsed;

            string elapsedTime = String.Format("{0:00}", ts.Milliseconds);
           Console.WriteLine("Operation took " + elapsedTime + "ms to complete");
        }

        public static void PrintFiles(string someFile)
        {
            Console.WriteLine(someFile);
        }
    }
}
