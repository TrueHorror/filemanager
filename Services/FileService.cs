﻿//using FileManager.BinaryTree;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace FileManager.Services
{
    class FileService
    {
        private const string RESOURCE_PATH = @"..\..\..\Resources\";
        private const string DRACULA_PATH = @"..\..\..\Resources\Dracula.txt";
        public static void ListAllFiles()
        {
            FileInfo[] fileList = GetAllFilesInDirectory(RESOURCE_PATH);
            Console.WriteLine("Listing files...");

            foreach (var file in fileList)
            {
                LoggingService.PrintFiles(file.Name);
            }
            
        }



        private static FileInfo[] GetAllFilesInDirectory(string path) // Returns list of FileInfo for all files in directory
        {
            DirectoryInfo di = new DirectoryInfo(path);

            return di.GetFiles();
        }

        public static void ListSpecificExtension(string extension)
        {

            FileInfo[] files = GetAllFilesInDirectory(RESOURCE_PATH);

            foreach (var file in files)
            {
                if (file.Extension == extension) LoggingService.PrintFiles(file.Name);
            }

        }

        private static FileInfo DraculaFileInfo()
        {
            return new FileInfo(DRACULA_PATH);
        }

        internal static void GetFileName()
        {
           LoggingService.PrintFiles(DraculaFileInfo().FullName);
        }

        internal static void GetFileSize()
        {
            LoggingService.PrintFiles((DraculaFileInfo().Length * 0.001) + "kb"); // converts to kb
        }

        internal static void GetTextLines()
        {
            int lines = 0;
            using(StreamReader sr = new StreamReader(DRACULA_PATH))
            {
                while(sr.ReadLine() != null)
                {
                    lines++;
                }
                LoggingService.PrintFiles("There are " + lines + " lines in the file.");
            }
           
        }

        internal static void SearchTerm() 
        {
            Console.Write("Search for: ");
            string word = Console.ReadLine();
            GetSearchedWord(word.ToLower().Trim());
        }

        internal static void GetSearchedWord(string word)
        {
            
            
            try
            {

                //Tree tree = new Tree(); Poor atempt to use a BinaryTree
                int count = 0;
                Stopwatch sw = LoggingService.StartLogging();
                using (StreamReader sr = new StreamReader(DRACULA_PATH))
                {
                    string line = null;
                    while((line = sr.ReadLine()) != null)
                    {
                        string[] words = line.Split(" ");
                        foreach (var fileword in words)
                        {
                            string formatted = Regex.Replace(fileword, "[^a-zA-Z]+", ""); // replaces everything that is special characters (not normal characters) 
                                                                                          // with empty string
                            if ((word.Length) != 0)
                            {
                                if (formatted.ToLower().Trim().Equals(word))
                                {
                                    count++;
                                }
                                //tree.insert(formatted.ToLower().Trim()); // Poor atempt to use a BinaryTree
                            }
                        }
                    }

                    
                    LoggingService.PrintFiles(word + " occurs " + count.ToString() + " times");
                    LoggingService.StopLogging(sw);
                    sr.Close();

                }


            }
            catch (FileNotFoundException e)
            {

                throw e;
            }
        }
    }
}
