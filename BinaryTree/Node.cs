﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.BinaryTree
{
    class Node
    {
        private Node left;
        private Node right;
        private int counter = 0;
        private string word;

        public Node(string word)
        {
            this.Left = null;
            this.Right = null;
            this.Counter = 1;
            this.Word = word;
        }

        public int Counter { get => counter; set => counter = value; }
        public string Word { get => word; set => word = value; }
        internal Node Left { get => left; set => left = value; }
        internal Node Right { get => right; set => right = value; }

        public string Write()
        {                
            return counter + " times";    
        }

        public void count()
        {
            counter++;
        }
    }
}
