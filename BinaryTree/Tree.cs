﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileManager.BinaryTree
{
    class Tree
    {
        private static Node root;
        private static int counter = 0;

        public static Node Root { get => root; }
        public static int Counter { get => counter; set => counter = value; }

        public void insert(string word)
        {
            if (root == null)
            {
                root = new Node(word);
                return;
            }

            Node currentNode = root;

            while (true)
            {
                if (currentNode.Word == word)
                {
                    if (currentNode.Left == null)
                    {
                        currentNode.Left = new Node(word);
                        return;
                    }
                    else
                    {
                        currentNode = currentNode.Left;
                    }

                }
                else
                {
                    if (currentNode.Right == null)
                    {
                        currentNode.Right = new Node(word);
                        return;
                    }
                    else
                    {
                        currentNode = currentNode.Right;
                    }
                    
                }
                return;
            }










        }

        public static int SearchAndCount(string word)
        {
            SearchAndCount(word, root);
            return Counter;

        }

        private static void SearchAndCount(string word, Node node)
        {

            if (node != null)
            {

                SearchAndCount(word, node.Left);
                
                    Console.WriteLine(node.Word);
                
                
                SearchAndCount(word, node.Right);




            }

        }
    }
}
